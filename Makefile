##
## Makefile for test in /home/grandr_r/asm
##
## Made by grandr_r
## Login   <grandr_r@epitech.net>
##
## Started on  Thu Feb 18 13:36:13 2016 grandr_r
## Last update Thu Mar 17 14:18:20 2016 grandr_r
##

NAME=		libasm.so

SRC=		src/memcpy.S \
		src/memmove.S \
		src/memset.S \
		src/rindex.S \
		src/strcasecmp.S \
		src/strchr.S \
		src/strcmp.S \
		src/strlen.S \
		src/strncmp.S \
		src/strpbrk.S \
		src/strspn.S \
		src/strstr.S

OBJ=		$(SRC:.S=.o)

RANLIB=		ranlib

CC=		gcc

CFLAGS +=	-fPIC

%.o: 	%.S
	nasm -f elf64 -o $@ $<

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CC) -shared -o $(NAME) $(OBJ)

clean:
		rm -f $(OBJ)

fclean:
		rm -f $(OBJ) $(NAME) *~

re:		fclean all
